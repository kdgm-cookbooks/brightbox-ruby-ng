name             "brightbox-ruby-ng"
maintainer       "Klaas Jan Wierenga"
maintainer_email "k.j.wierenga@gmail.com"
license          "MIT"
description      "Installs/Configures brightbox-ruby-ng"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.1.5"

supports "ubuntu"

depends "apt"
