#
# Cookbook Name:: brightbox-ruby-ng
# Recipe:: passenger
#
# Copyright (C) 2013 Klaas Jan Wierenga
# 
# All rights reserved - Do Not Redistribute
#
# Install passenger package appropriate for specified ruby version
#
# See "Brightbox Wiki/Next Generation Ubuntu Ruby Packages"
# https://www.brightbox.com/docs/ruby/ubuntu/ 
#
include_recipe 'brightbox-ruby-ng::default'

case node['brightbox-ruby-ng']['ruby']
when 'ruby1.8'
  package 'passenger-common' do
    action :install
  end

  # remove passenger-common1.9.1
  # otherwise otherwise passenger (1.8) won't load
  package 'passenger-common1.9.1' do
    action :remove
  end
when 'ruby1.9.3'
  package 'passenger-common1.9.1' do
    action :install
  end
else
  raise "Invalid attribute value '#{node['brightbox-ruby-ng']['version']}' for node['brightbox-ruby-ng']['version']"
end
