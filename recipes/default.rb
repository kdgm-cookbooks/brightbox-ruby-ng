#
# Cookbook Name:: brightbox-ruby-ng
# Recipe:: default
#
# Copyright (C) 2013 Klaas Jan Wierenga
#
# All rights reserved - Do Not Redistribute
#
#
# Setup Launchpad package repository from Brightbox.
# Install Ruby Enterprise Edition ruby1.8 or ruby1.9.3 from Brightbox packages.
#
# See "Brightbox Wiki/Next Generation Ubuntu Ruby Packages"
# https://www.brightbox.com/docs/ruby/ubuntu/
#
include_recipe 'apt'

package 'python-software-properties'

apt_repository 'brightbox-ruby-ng' do
  uri "http://ppa.launchpad.net/brightbox/ruby-ng/ubuntu"
  distribution node['lsb']['codename']
  components ['main']
  keyserver 'keyserver.ubuntu.com'
  key 'C3173AA6'
end

apt_preference "brightbox" do
  glob "*"
  pin "origin ppa.launchpad.net"
  pin_priority "700"
end

ruby_block "inspect-languages-ruby" do
  block do
    Gem.clear_paths
    Chef::Log.info node['languages']['ruby'].inspect
  end
  action :nothing
end

ohai "reload" do
  action :nothing
  notifies :create, 'ruby_block[inspect-languages-ruby]', :immediately
end

case node['brightbox-ruby-ng']['ruby']
when 'ruby1.8'
  %w(ruby rubygems ruby-switch).each do |pkg|
    package pkg
  end

  execute 'ruby-switch --set ruby1.8' do
    notifies :reload, 'ohai[reload]', :immediately
  end
when 'ruby1.9.3'
  %w(ruby1.9.3 rubygems ruby-switch).each do |pkg|
    package pkg
  end

  # note that Ruby 1.9.3 shows as 1.9.1, due to a historical
  # Debian thing about binary compatibility
  execute 'ruby-switch --set ruby1.9.1' do
    notifies :reload, 'ohai[reload]', :immediately
  end

when /\A(ruby2\.\d)\z/
  version = $1  # ruby2.X from regex
  %W(#{version} #{version}-dev ruby-switch).each do |pkg|
    package pkg
  end
  if 'ubuntu' == node['platform'] && '12.04' == node['platform_version']
    package "rubygems"
  end

  execute "ruby-switch --set #{version}" do
    notifies :reload, 'ohai[reload]', :immediately
  end

else
  raise "Invalid attribute value '#{node['brightbox-ruby-ng']['ruby']}' for node['brightbox-ruby-ng']['ruby']"
end
