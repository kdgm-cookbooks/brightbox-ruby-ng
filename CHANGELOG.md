## Release 0.1.5
- [enh] Only install rubygems on old ubuntu 12.04 instances

## Release 0.1.4
- [enh] Allow other versions of ruby2.X
