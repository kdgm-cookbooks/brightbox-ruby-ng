# brightbox-ruby-ng cookbook

Cookbook to install Brightbox Next Generation Ubuntu Ruby Packages and Passenger.

See https://www.brightbox.com/docs/ruby/ubuntu/

# Requirements

Supports Ubuntu only. Depends on the 'apt' cookbook.

# Usage

This cookbook will install ruby and (optionally) passenger packages from the
Brightbox Launchpad package repository (`ppa:brightbox/ruby-ng`).

By default this cookbook will install ruby version 1.9.3 packages.

# Attributes

Specify the ruby version to install.

	default['brightbox-ruby-ng']['ruby'] = 'ruby1.9.3'

Valid values are `ruby1.9.3` and `ruby1.8`.

# Recipes

## default

Install `ruby1.9.3` or `ruby` (for 1.8), `rubygems` and `ruby-switch`.

## passenger
Include default recipe and install `passenger-common1.9.1` (for 1.9.3) or `passenger-common` (for 1.8)

# Author

Author:: Klaas Jan Wierenga (<k.j.wierenga@gmail.com>)
