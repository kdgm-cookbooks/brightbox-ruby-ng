#
# Set version of ruby to install and use
# Valid values are: ruby1.8, ruby1.9.3
#
default['brightbox-ruby-ng']['ruby'] = 'ruby1.9.3'